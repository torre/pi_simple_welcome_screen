Raspberry PI simple welcome screen 
=========================

This bash script provides an image that contains network information configured on Raspberry PI.

It is intended to be shown on a small display (like the [Kuman one](http://kumantech.com/kuman-35-inch-tft-lcd-display-480x320-rgb-pixels-touch-screen-monitor-for-raspberry-pi-3-2-model-b-b-a-a-module-spi-interface-with-touch-pen-sc06_p0014.html)) to state the device current network configuration.

This script has to be added to `rc.local` in order to show the data through [fbi](https://www.kraxel.org/blog/linux/fbida/).

It relies on [ImageMagick](http://imagemagick.org/script/index.php)  to create the image that will be shown.

Network data is obtained by standard shell network utilies that usually are shipped with Linux distributions.