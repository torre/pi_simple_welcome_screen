#!/bin/bash
#
# Script to generate a final boot splash with Network Information for Raspberry
# Mainly used on a 3.5 inch screen to provide information for support
#
# This script relies on imagemagick and fbi
#
# by Gian Luca Dalla Torre - 12/28/2016
#

. /etc/profile

SRC_IMAGE="/opt/boot/boot.png"
FINAL_IMAGE="/opt/boot/boot_final.png"

if [ -f $FINAL_IMAGE ]; then
  rm -rf $FINAL_IMAGE
fi

# Detect main interface and IP with NetMask
# Inspiration taken from: https://www.cyberciti.biz/faq/bash-shell-command-to-find-get-ip-address/

MAIN_IF=`ip -f inet -o addr show | grep scope | grep global | awk '{print $2}'`
IP_NM=`ip -f inet -o addr show | grep scope | grep global | awk '{print $4}'`

# Detect default route
# Inspiration taken from: http://stackoverflow.com/questions/1204629/how-do-i-get-the-default-gateway-in-linux-given-the-destination

DEF_ROUTE=`ip route | awk '/default/ { print $3 }'`

# My idea for primary DNS
DNS=`cat /etc/resolv.conf | grep nameserver | awk '{print $2}' | head -1`

echo ""
echo "Main interface is: $MAIN_IF"
echo "IP with Netmask is: $IP_NM"
echo "Default Route is: $DEF_ROUTE"
echo "Primary DNS is: $DNS"
echo ""

# Now convert to images
DEFAULT_TEXT="IP: $IP_NM on $MAIN_IF \nRoute: $DEF_ROUTE\nDNS: $DNS"

convert "$SRC_IMAGE" -font "/usr/share/fonts/truetype/liberation/LiberationSerif-Bold.ttf" -weight 400 -gravity Center -pointsize 30 -fill white -annotate +0+30 "$DEFAULT_TEXT" "$FINAL_IMAGE"

# Show the image on correct FrameBuffer and terminal
# Since I am using Kuman 3.5 LCD with its driver, it is identified by fb1 with vt1

fbi -T 1 -noverbose "$FINAL_IMAGE" &
